#!/bin/bash

set -e
# Move and test loop
while [ "$(ls -A test_set/good)" ]; do
  file=$(ls test_set/good/*.tex | head -n 1)
  mv "$file" test.tex
  bash clear_and_build.sh
  ./cmake-build-debug/tex-preprocessor && echo test passed
done

echo ----
echo test good passed
echo ---

while [ "$(ls -A test_set/error)" ]; do
  file=$(ls test_set/error/*.tex | head -n 1)
  mv "$file" test.tex
  bash clear_and_build.sh

  set +e
  ./cmake-build-debug/tex-preprocessor
  exit_code=$?
  set -e

  if [ $exit_code -eq 0 ]; then
    echo "No error detected in this test. Aborting loop."
    break
  else
    echo test passed
  fi
done

echo ----
echo test error passed
echo ---